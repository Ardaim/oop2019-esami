package a02b.e2;

import java.util.HashSet;
import java.util.Set;

public class LogicsImpl implements Logics {
    private int size;
    private Set<Pair<Integer, Integer>> cells;

    public LogicsImpl(int size) {
        this.cells = new HashSet<>();
    }

    @Override
    public void addCell(Pair<Integer, Integer> cell) {
        if (!cells.contains(cell)) {
            cells.add(cell);
        }
    }

    @Override
    public Set<Pair<Integer, Integer>> moveCells() {

        for (Pair<Integer, Integer> pair : cells) {
            if (pair.getY()<size-1) {
                pair = new Pair<>(pair.getX(), pair.getY() + 1);
            }
            
        }
        return this.cells;
    }

}
