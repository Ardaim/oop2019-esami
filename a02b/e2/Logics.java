package a02b.e2;

import java.util.Set;

public interface Logics {

    void addCell(Pair<Integer, Integer> cell);

    Set<Pair<Integer, Integer>> moveCells();

}