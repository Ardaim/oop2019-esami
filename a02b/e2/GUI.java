package a02b.e2;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class GUI extends JFrame {

    private static final long serialVersionUID = -6218820567019985015L;
    private final Logics model;
    private final JPanel panel;
    private final int size;
    private Map<JButton, Pair<Integer, Integer>> buttonmap;

    

    public GUI(int size) {
        this.model = new LogicsImpl(size);
        this.size = size;
        this.buttonmap = new HashMap<>();
        
        this.panel = new JPanel(new GridLayout(size, size));
        for (int y = 0; y < this.size; y++) {
            for (int x = 0; x < this.size; x++) {
                JButton button = this.createGridButton();
                panel.add(button);
                buttonmap.put(button, new Pair<>(x, y));
            }
        }
        
        JPanel panel2= new JPanel();
        panel2.add(this.createArrowButton());
        
        this.getContentPane().setLayout(new BorderLayout());
        this.getContentPane().add(panel, BorderLayout.CENTER);
        this.getContentPane().add(panel2, BorderLayout.SOUTH);

        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(size * 100, size * 100);
        this.setResizable(false);
        this.setLocationByPlatform(true);
        this.setVisible(true);

    }

    private JButton createGridButton() {
        JButton bt = new JButton();
        bt.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                bt.setText("X");
                model.addCell(buttonmap.get(bt));

            }
        });
        return bt;
    }
    
    private JButton createArrowButton() {
        JButton bt2=new JButton(">");
        bt2.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent e) {
                for (Entry<JButton, Pair<Integer, Integer>> entry : buttonmap.entrySet()) {
                    for (Pair<Integer, Integer> pair : model.moveCells()) {
                        if (entry.getValue().equals(pair)) {
                            entry.getKey().setText("X");
                        }
                        else {
                            entry.getKey().setText("");
                        }
                        
                    }
                }
                panel.validate();
            }
        });
        return bt2;
    }
}
