package a01a.e2;

public interface Logics {

    boolean isHit(Pair<Integer, Integer> pos);

    boolean isOver();

}