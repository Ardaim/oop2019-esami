package a01a.e2;

import javax.swing.*;
import java.util.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GUI extends JFrame {

    /**
     * 
     */
    private static final long serialVersionUID = -2716745841904810384L;
    private final Logics model;
    private final Map<JButton, Pair<Integer, Integer>> buttonmap;
    private final int size;
    private final JPanel panel;

    public GUI(int size, int boat) {
        this.size = size;
        this.model = new LogicsImpl(size, boat);
        this.buttonmap = new HashMap<>();
        this.panel = new JPanel(new GridLayout(size, size));
        this.getContentPane().add(panel);

        for (int y = 0; y < this.size; y++) {
            for (int x = 0; x < this.size; x++) {
                JButton button = this.createButton();
                this.panel.add(button);
                this.buttonmap.put(button, new Pair<>(x, y));
            }
            ;

        }
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(size * 100, size * 100);
        this.setResizable(false);
        this.setLocationByPlatform(true);
        this.setVisible(true);

    }

    private JButton createButton() {
        JButton bt = new JButton();
        bt.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                bt.setEnabled(false);
                if (model.isHit(buttonmap.get(e.getSource()))) {
                    bt.setText("X");
                } else {
                    bt.setText("O");
                }

                if (model.isOver()) {
                    System.exit(0);
                }
            }
        });
        return bt;
    }

}
