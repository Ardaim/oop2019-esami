package a01a.e2;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import a01a.e2.Pair;

public class LogicsImpl implements Logics {

    private static final int TRIES = 5;
    private final int gridsize;
    private final int boatsize;
    private int shotsFired;
    private final Set<Pair<Integer, Integer>> boatCoordinates = new HashSet<>();
    private Set<Pair<Integer, Integer>> boatHits = new HashSet<>();;

    public LogicsImpl(int gridsize, int boatsize) {
        this.gridsize = gridsize;
        this.boatsize = boatsize;
        this.shotsFired = 0;
        this.setBoatposition();
    }

    private void setBoatposition() {
        int xpos = new Random().nextInt(gridsize - boatsize + 1);
        final int ypos = new Random().nextInt(gridsize);

        for (int i = 0; i < boatsize; i++) {
            boatCoordinates.add(new Pair<>(xpos++, ypos));

        }

    }

    @Override
    public boolean isHit(Pair<Integer, Integer> pos) {
        this.shotsFired++;
        if (boatCoordinates.contains(pos) & !boatHits.contains(pos)) {
            boatHits.add(pos);
            return true;
        }
        return false;
    }

    @Override
    public boolean isOver() {
        if (boatHits.size() >= boatsize) {
            System.out.print("Hai vinto");
            return true;
        }
        if(shotsFired >= TRIES) {
            System.out.print("Hai perso");
            return true;
        }
        return false;
    }
}
