package a01a.e1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

public class GraphFactoryImpl implements GraphFactory {

    public GraphFactoryImpl() {
        // TODO Auto-generated constructor stub
    }

    @Override
    public <X> Graph<X> createDirectedChain(List<X> nodes) {
        Stream<Pair<X, X>> edgestream = Stream.iterate(0, x -> x + 1).limit(nodes.size() - 1)
                .map(x -> new Pair<>(nodes.get(x), nodes.get(x + 1)));

        return new GraphImpl<X>(edgestream);
    }

    @Override
    public <X> Graph<X> createBidirectionalChain(List<X> nodes) {
        Stream<Pair<X, X>> edgestream = Stream.iterate(0, x -> x + 1).limit(nodes.size() - 1).flatMap(
                x -> Stream.of(new Pair<>(nodes.get(x), nodes.get(x + 1)), new Pair<>(nodes.get(x + 1), nodes.get(x))));

        return new GraphImpl<X>(edgestream);
    }

    @Override
    public <X> Graph<X> createDirectedCircle(List<X> nodes) {
        List<X> nodesWithEnd=new ArrayList<>(nodes);
        nodesWithEnd.add(nodes.get(0));
        
        return this.createDirectedChain(nodesWithEnd);
    }

    @Override
    public <X> Graph<X> createBidirectionalCircle(List<X> nodes) {
        List<X> nodesWithEnd=new ArrayList<>(nodes);
        nodesWithEnd.add(nodes.get(0));
        return createBidirectionalChain(nodesWithEnd);
    }

    @Override
    public <X> Graph<X> createDirectedStar(X center, Set<X> nodes) {
        Stream<Pair<X, X>> edgestream = nodes.stream().map(x->new Pair<>(center, x));
        return new GraphImpl<>(edgestream);
    }

    @Override
    public <X> Graph<X> createBidirectionalStar(X center, Set<X> nodes) {
        Stream<Pair<X, X>> edgestream = nodes.stream().flatMap(x->Stream.of(new Pair<X, X>(center, x), new Pair<>(x, center)));
        return new GraphImpl<>(edgestream);
    }

    @Override
    public <X> Graph<X> createFull(Set<X> nodes) {
        Stream<Pair<X, X>> edgestream = nodes.stream().flatMap(x->nodes.stream().filter(y->y!=x).map(y->new Pair<>(x, y)));
        return new GraphImpl<>(edgestream);
    }

    @Override
    public <X> Graph<X> combine(Graph<X> g1, Graph<X> g2) {
        // TODO Auto-generated method stub
        return null;
    }

}
