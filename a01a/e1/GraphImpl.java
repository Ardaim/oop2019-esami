package a01a.e1;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class GraphImpl<X> implements Graph<X> {

    private Set<Pair<X, X>> edges;

    public GraphImpl(Stream<Pair<X, X>> edgestream) {
        this.edges = edgestream.collect(Collectors.toSet());
    }

    @Override
    public Set<X> getNodes() {
        // TODO Auto-generated method stub
        return this.edges.stream().flatMap(e->Stream.of(e.getX(), e.getY())).distinct().collect(Collectors.toSet());
    }

    @Override
    public boolean edgePresent(X start, X end) {
        // TODO Auto-generated method stub
        return this.edges.contains(new Pair<X, X>(start, end));
    }

    @Override
    public int getEdgesCount() {
        // TODO Auto-generated method stub
        return this.edges.size();
    }

    @Override
    public Stream<Pair<X, X>> getEdgesStream() {
        // TODO Auto-generated method stub
        return this.edges.stream();
    }

}
