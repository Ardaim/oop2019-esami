package a01b.e2;

public interface Logics {

    boolean isMineHit(Pair<Integer, Integer> pos);

    boolean isWin();

    Pair<Integer, Integer> showMineNearby(Pair<Integer, Integer> pos);

}