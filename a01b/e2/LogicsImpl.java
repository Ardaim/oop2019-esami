package a01b.e2;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class LogicsImpl implements Logics {

    private final int mineCount;
    private final int emptyCells;
    private int emptyCellsHit;
    private final int gridsize;
    private Set<Pair<Integer, Integer>> minepos;

    public LogicsImpl(int gridsize, int mines) {
        this.gridsize = gridsize;
        this.emptyCells = gridsize * gridsize - mines;
        this.emptyCellsHit = 0;
        this.mineCount = mines;
        this.minepos = new HashSet<>();
        this.setMinesPos();
    }

    private void setMinesPos() {
        final Random rnd = new Random();
        while (minepos.size() < mineCount) {
            Pair<Integer, Integer> var = new Pair<>(rnd.nextInt(gridsize), rnd.nextInt(gridsize));
            if (!minepos.contains(var)) {
                minepos.add(var);
                System.out.print(var.toString());
            }
        }
    }

    @Override
    public boolean isMineHit(Pair<Integer, Integer> pos) {
        if (minepos.contains(pos)) {
            System.out.print("Hai perso");
            return true;
        }
        this.emptyCellsHit++;
        return false;
    }

    @Override
    public boolean isWin() {
        if (emptyCellsHit == emptyCells) {
            System.out.print("Hai vinto");
            return true;
        }
        return false;
    }

    @Override
    public Pair<Integer, Integer> showMineNearby(Pair<Integer, Integer> pos) {
        // qua ci andrà un return nul se non cè nessuna mina vicina e nel action
        // listener un handler per la cosa
        // if(var=showMineNearby!=null){tutto il resto per trovare il corrispondente bt
        // (key) dato var (value)}
        // poi dopo dal bt trovato si chiamerà settext("icona mina")
        for (int y = pos.getY() - 1; y <= pos.getY() + 1; y++) {
            for (int x = pos.getX() - 1; x <= pos.getX() + 1; x++) {
                Pair<Integer, Integer> var = new Pair<>(x, y);
                if (minepos.contains(var)) {
                    return var;
                }
            }

        }
        return null;
    }

}
