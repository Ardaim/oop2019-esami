package a01b.e2;

import javax.swing.*;
import java.util.*;
import java.util.Map.Entry;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GUI extends JFrame {

    /**
     * 
     */
    private static final long serialVersionUID = 8427462392269280741L;
    private final Logics model;
    private final int size;
    private Map<JButton, Pair<Integer, Integer>> buttonmap;
    private JPanel panel;

    public GUI(int size, int mines) {
        this.model = new LogicsImpl(size, mines);
        this.size = size;
        this.buttonmap = new HashMap<>();
        this.panel = new JPanel(new GridLayout(size, size));
        this.getContentPane().add(panel);

        for (int y = 0; y < this.size; y++) {
            for (int x = 0; x < this.size; x++) {
                JButton button = this.createButton();
                this.panel.add(button);
                buttonmap.put(button, new Pair<>(x, y));
            }
        }

        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(size*100, size*100);
        this.setResizable(false);
        this.setLocationByPlatform(true);
        this.setVisible(true);

    }

    private JButton createButton() {
        JButton bt = new JButton();
        bt.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                bt.setEnabled(false);
                Pair<Integer, Integer> btCoord = buttonmap.get(e.getSource());
                if (model.isMineHit(btCoord)) {
                    bt.setText("X");
                    System.exit(0);
                } else {
                    bt.setText("O");
                    Pair<Integer, Integer> nearbyCoord = model.showMineNearby(btCoord);
                    if (nearbyCoord != null) {
                        for (Entry<JButton, Pair<Integer, Integer>> entry : buttonmap.entrySet()) {
                            if (entry.getValue().equals(nearbyCoord))
                                entry.getKey().setText("X");
                        }
                    }
                }
                if (model.isWin()) {
                    System.exit(0);
                }
            }

        });
        return bt;
    }
}
